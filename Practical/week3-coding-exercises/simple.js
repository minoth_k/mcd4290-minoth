// Question 1 code
/* 
  * @desc -  Outputs the property and property values in String suitable
  for rendering as HTML
  * @param - 'object', the object that is to be converted.
*/
 
function objectToHTML(object)
{
    let output = "";
    for (let prop in object)
    {
        output += `${prop}: ${object[prop]} \n`
    }
    return output 
}

// Testing Q1
var testObj = {
    number : 1, 
    string : "abc",
    array : [5,4,3,2,10],
    boolean : true
};

let outPutArea = document.getElementById("outputArea1")
outPutArea.innerText = objectToHTML(testObj); 

// Question 2
var outputAreaRef = document.getElementById("outputArea2");
var output = "";
function flexible(fOperation, operand1, operand2)
{
 var result = fOperation(operand1, operand2);
 return result;
}

function addTwoNum(num1,num2)
{
    return num1 + num2;
}

var multTwoNum = function(num1,num2) {return num1 * num2};

output += flexible(addTwoNum, 3, 5) + "<br/>";
output += flexible(multTwoNum, 3, 5) + "<br/>"; 
outputAreaRef.innerHTML = output;
 
// Question 3 - Pseudocode
/* 
 * Initialize the function that takes in the array as a parameter 
 * Set minimum and maximum variables equal to the first element of the array
 * Initialize a for loop to cycle through the array 
  * For loop starts from 2nd element onwards as first has been checked already
  * Compare the array[index] to the current min, if array[index] < min then min = array[index]
  * Compare the array[index] to the current max, if array[index] > max then max = array[index]
 * End the loop 
 * Return the values of min and max
 * End the function
* Output the values of min and max to the HTML output area 
*/

// Question 4 
function minMaxNumsOfArray(array) 
{
    let min = array[0];
    let max = array[0];
    for (i = 1; i < array.length; i++)
    {
        if (array[i] < min) 
        {
            min = array[i];
        }
        if (array[i] > max)
        {
            max = array[i];
        }
    }
    return `${max} \n <br/> ${min}`;
}

// Testing Q4
var values = [4, 3, 6, 12, 1, 3, 8];
var outputAreaRef = document.getElementById("outputArea3");
outputAreaRef.innerHTML = minMaxNumsOfArray(values); 