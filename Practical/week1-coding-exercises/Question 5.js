// NOTE that this is the corrected code.
let year;
let yearNot2015Or2016;
year = 2000; 
yearNot2015Or2016 = year !== 2015 && year !== 2016;
console.log(yearNot2015Or2016);

/*
 The incorrect bit was, yearNot2015Or2016 = year !== 2015 || year !== 2016
 as it uses the OR operator which means yearNot2015or2016 becomes true when
 year is 2015 and 2016 due to one condition being true. Using AND solves this issue
 */ 