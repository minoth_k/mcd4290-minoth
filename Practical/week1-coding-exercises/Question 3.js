// Creating the object 
var details = {
  firstName: "Kanye",
  lastName: "West",
  birthDate: "8 June 1977",
  annualIncome: 150000000.00
};

// Output to console
console.log(details.firstName + " " + details.lastName + " was born on " + details.birthDate + " and has an annual income of $" +details.annualIncome.toFixed(0));

/* Alternatively,
change birthDate from String to Date to this: birthDate: new Date("08 June 1977")
console.log(`${details.firstName} ${details.lastName} was born on ${details.birthDate.getDate()} June ${details.birthDate.getFullYear()} and has an annual income of $${details.annualIncome.toFixed(0)}`);
*/ 
