// Let 'r' and c' be the respective variables for the radius and circumference of the circle.

var r = 4; // Assigning the value for the radius within its variable
var c = 2 * Math.PI * r;
c = c.toFixed(2); // Converts the number to 2 decimal places.
console.log("The circumference of the circle with radius " + r + ", is " + c);

/* 
Or we can use the following,
console.log(`The circumference of the circle with radius, ${r} is ${(2 * Math.PI * r).toFixed(2)}`);
*/



