var animalString = "cameldogcatlizard";
var andString = " and ";

// By using substr()
console.log(`${animalString.substr(8,3)}${andString}${animalString.substr(5,3)}`);

// By using substring();
console.log(`${animalString.substring(8,11)}${andString}${animalString.substring(5,8)}`)

