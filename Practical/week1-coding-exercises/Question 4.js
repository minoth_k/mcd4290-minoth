var number1, number2;
//RHS generates a random number between 1 and 10 inclusive
number1 = Math.floor((Math.random() * 10) + 1);

//RHS generates a random number between 1 and 10 inclusive
number2 = Math.floor((Math.random() * 10) + 1);
console.log("number1 = " + number1 + " number2 = " + number2);

//HERE your code to swap the values in number1 and number2

//Creating a temporary variable to swap the values with one being overwritten and hence lost.
var temp = number1;
 
number1 = number2;
number2 = temp;

console.log("number1 = " + number1 + " number2 = " + number2);