//to check the code, uncomment the corresponding question.

question1();
question2();
question3();
question4();
question5();

function question1(){
    let output = ""; //empty output, fill this so that it can print onto the page.
    
    //Question 1 here 
    let myArr = [54, -16, 80, 55, -74, 73, 26, 5, -34, -73, 19, 63, -55, -61, 65, -14, -19, -51,-17,-25];
    let positiveOdd = [] ;
    let negativeEven = [] ;
    for (var i=0; i < myArr.length; i++) 
    {
        if ((myArr[i] >= 0) && ((myArr[i] % 2) != 0)) 
        {
         positiveOdd.push(myArr[i]);  
        }
        else
        {
            if ((myArr[i] < 0) && ((myArr[i] % 2) == 0))
            {
                negativeEven.push(myArr[i]);
            }
        }
    }
    output = `Positive odd: ${positiveOdd} \n Negative even: ${negativeEven}`
    
    let outPutArea = document.getElementById("outputArea1") //this line will find the element on the page called "outputArea1"
    outPutArea.innerText = output //this line will fill the above element with your output.
}

function question2(){
    let output = "";
    
    //Question 2 here 
    var oneCount = 0;
    var twoCount = 0;
    var threeCount = 0;
    var fourCount = 0;
    var fiveCount = 0;
    var sixCount = 0;
    
    for (var i=0; i < 60000; i++) 
    {
        var diceRoll = (Math.floor((Math.random() * 6) + 1));
        if (diceRoll == 1) 
        {
            oneCount ++;
        } 
        else 
        {
            if (diceRoll == 2)
            {
               twoCount ++; 
            }
            else 
            {
                if (diceRoll == 3) 
                {
                    threeCount ++;
                }
                else 
                {
                    if (diceRoll == 4)
                    {
                        fourCount ++;
                    }
                    else 
                    {
                        if (diceRoll == 5)
                        {
                            fiveCount ++;
                        }
                        else 
                        {
                            if (diceRoll == 6)
                            {
                                sixCount ++;
                            }
                        }
                    }
                }
            }
        }
    }

    
    output += `Frequency of die rolls\n1: ${oneCount}\n2: ${twoCount}\n3: ${threeCount}\n4: ${fourCount}\n5: ${fiveCount}\n6: ${sixCount}`
    
    let outPutArea = document.getElementById("outputArea2") 
    outPutArea.innerText = output 
}

function question3(){
    let output = "Frequency of die rolls\n" 

    //Question 3 here
    let countArr = ["",0,0,0,0,0,0]; 
    for (var i=0; i < 60000; i++) 
    {
      let diceRoll = (Math.floor((Math.random() * 6) + 1));
      countArr[diceRoll] ++ ;
    }
    
    for (i=1; i < countArr.length; i++)
    {
        output += `${i}:${countArr[i]}\n`
    }
    
    let outPutArea = document.getElementById("outputArea3")
    outPutArea.innerText = output 
}

function question4(){
    let output = "" 
    
    //Question 4 here 
    var dieRolls = {
        Frequencies: {
            1:0,
            2:0,
            3:0,
            4:0,
            5:0,
            6:0,
        },
        Total:60000,
        Exceptions:""
    }
    for (var i=0; i < dieRolls.Total; i++)
    {   
        let diceValue = (Math.floor((Math.random() * 6) + 1));
        dieRolls.Frequencies[diceValue] ++;
    }
    for (i=1; i <= Object.keys(dieRolls.Frequencies).length; i++ )
    {
        if (dieRolls.Frequencies[i] > 1.01*10000 || dieRolls.Frequencies[i] < 0.99*10000)
        {
            dieRolls.Exceptions += `${i} `
        }
    }
    output += `Frequency of dice rolls\n Total Rolls: ${dieRolls.Total}\n`;
    for (let prop in dieRolls.Frequencies)
    {
        output += `${prop}: ${dieRolls.Frequencies[prop]}\n`
    } 
    output += `Exceptions: ${dieRolls.Exceptions}`;                    
    let outPutArea = document.getElementById("outputArea4") 
    outPutArea.innerText = output 
}

function question5(){
    let output = "" 
    
    //Question 5 here 
    let person = {
        name: "Jane",
        income: 127050,
    }
    if (person.income >= 180001) 
    {
        person.taxOwed = 54097 + ((person.income - 180000)*0.45);  
    }
    else 
    {
        if (person.income > 90000 && person.income <= 180000)
        {
            person.taxOwed = 20797 + ((person.income - 90000)*0.37);
        } 
        else 
        {
            if (person.income > 37000 && person.income < 90001)
            {
               person.taxOwed = 3572 + ((person.income - 37000)*0.325); 
            }
            else
            {
                if (person.income > 18200 && person.income < 37001)
                {
                   person.taxOwed = (person.income - 18200)*0.325 ;
                }
                else 
                {
                    if (person.income < 18200) 
                    {
                        person.taxOwed = 0;
                    }
                }
            }
        } 
    }
    output += `${person.name}'s income is: ${person.income}, and her tax owed is: $${person.taxOwed.toFixed(2)}`
    let outPutArea = document.getElementById("outputArea5") 
    outPutArea.innerText = output 
}

