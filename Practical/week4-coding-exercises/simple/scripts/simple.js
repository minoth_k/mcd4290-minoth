function doIt()
{
    let number1Ref = document.getElementById('number1');
    let number2Ref = document.getElementById('number2');
    let number3Ref = document.getElementById('number3');
    let answerRef = document.getElementById('answer');
    let number1 = number1Ref.value;
    let number2 = number2Ref.value;
    let number3 = number3Ref.value;
    let answer = `${parseInt(number1) + parseInt(number2) + parseInt(number3)}`;
    
    if (answer >= 0) 
    {
        answerRef.className = 'positive';
    }
    if (answer < 0) 
    {
        answerRef.className = 'negative';
    }
    
    let oddOrEvenRef = document.getElementById("odd/even");
    
    if (answer % 2 == 0)
    {
        oddOrEvenRef.className = 'even';  
        oddOrEvenRef.innerHTML = " (even)";
    }
    
    if (answer % 2 != 0)
    {
        oddOrEvenRef.className = 'odd';
        oddOrEvenRef.innerHTML = " (odd)";
    }
    answerRef.innerHTML = answer; 
}